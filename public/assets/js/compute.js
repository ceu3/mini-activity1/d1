function compute() {
    let base = Number(document.getElementById('base').value);
    let height = Number(document.getElementById('height').value);

    if (isNaN(base) || isNaN(height)) {
      document.getElementById('invalid_result').innerHTML = "Invalid input!";
      document.getElementById('area').innerHTML = null;
      document.getElementById('equal').innerHTML = null;
      document.getElementById('result').innerHTML = null;
    }else if (base == "" || height == ""){
      document.getElementById('invalid_result').innerHTML = "Invalid input!";
      document.getElementById('area').innerHTML = null;
      document.getElementById('equal').innerHTML = null;
      document.getElementById('result').innerHTML = null;
    }else{
      document.getElementById('result').innerHTML = base * height;
    }

    return false;
   }

function clear() {
    document.getElementById("base").value = null;
    document.getElementById("height").value = null;
    document.getElementById("invalid_result").innerHTML = null;
    document.getElementById('area').innerHTML = "A"; 
    document.getElementById('equal').innerHTML = " = ";
    document.getElementById('result').innerHTML = "b h";
   }
    
    document.getElementById('compute').addEventListener('click', compute);
    document.getElementById('clear').addEventListener('click', clear);